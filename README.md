# awesome-lib

## Javascript

*  https://lodash.com/ : Great utility library with lot of helpers for simplify code
*  https://github.com/axios/axios : Promise based HTTP client for the browser and node.js
*  https://github.com/gnab/remark : Lib JS pour réaliser des slideshow en HTML/CSS

## APIs utiles pour sandbox

* https://www.themoviedb.org/documentation/api : base de données de films exposées sous forme d'API  
* https://swapi.co/ : api sur les films star wars

## Ebook

* https://olegkrivtsov.github.io/using-zend-framework-3-book/html/ : Zend framework 3

## Blog

*  https://joostvanveen.com/blog : Excellent blog notamment pour la configuration mac d'un environnement de développement

## Tools Windows

*  https://mobaxterm.mobatek.net/ : client ssh mobile sans installation pratique pour faire du ssh sur une machine sans les droits adminstrateurs.

## Tools Linux

* https://httpie.org/ : un outil pour faire du http en ligne de commande (alternative à wget et curl)
